﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TestWCFLib.Calculator
{
    [ServiceContract]
    public interface ICalculator
    {
        [OperationContract]
        int Sum(int n1, int n2);

        [OperationContract]
        int Sub(int n1, int n2);

        [OperationContract]
        int Mul(int n1, int n2);

        [OperationContract]
        double Div(int n1, int n2);
    }
}
